
  /**
   * @file Gruntfile.js
   *
   * Simply run "grunt" from your terminal to have your minion start doing
   * your bidding.
   *
   * @package RapidLocalDeployment
   * @link https://bitbucket.org/jklatt86/rapid-local-deployment
   * @author Jimmy K. <jimmy@ghazlawl.com>
   * @version 2.0.2
   */

  var timer = require("grunt-timer");

  module.exports = function(grunt) {

    // Create a space for our functions and vars to live.
    grunt.ghazlawl = {
      version: "v2.0.3",
      vars: grunt.option('vars') ? grunt.file.readJSON(grunt.option('vars')) : null
    };

    /**
     * Get the list of vars files.
     *
     * @return array
     * @author Jimmy K. <jimmy@ghazlawl.com>
     * @since 2.0.1
     */

    grunt.ghazlawl.getVarsFileList = function()
    {

      var files = grunt.file.expandMapping(["**/*.json"], "", { cwd: "vars/" });
      var filenames = [];

      files.forEach(function(element, index, array) {
        filenames.push(element.dest);
      });

      return filenames;

    }

    /**
     * Get a random form of address. Used by the minion.
     *
     * @return string
     * @author Jimmy K. <jimmy@ghazlawl.com>
     * @since 2.0.1
     */

    grunt.ghazlawl.getMinionAddressText = function()
    {

      // Hold the forms of address.
      var addresses = [
        "my lord",
        "sire",
      ];

      // Get a random form of address.
      var randomAddress = addresses[Math.floor(Math.random() * addresses.length)];

      return randomAddress;

    }

    /**
     * Get a random joke (even if it's not funny.)
     *
     * @return string
     * @author Jimmy K. <jimmy@ghazlawl.com>
     * @since 2.0.1
     */

    grunt.ghazlawl.getRandomJoke = function()
    {

      // Hold the jokes.
      var jokes = [];
      jokes.push("99 little bugs in the code, 99 bugs in the code, patch one down, compile it around, 138 bugs in the code.");
      jokes.push("What's the object-oriented way to become wealthy? Inheritance.");
      jokes.push("A wife calls her programmer husband and tells him, 'While you're out, buy some milk.' He never returns home.");
      jokes.push("Ask a programmer to review 10 lines of code, he'll find 10 issues. Ask him to do 500 lines and he'll say it looks good.");
      // jokes.push("A man says, 'I told him I can't open the jar.' His friend says, 'Download and install Java.'");
      jokes.push("Sometimes it pays to stay in bed on Monday, rather than spending the rest of the week debugging Monday's code.");
      jokes.push("Perl - The only language that looks the same before and after encryption.");
      jokes.push("I don't always use recursion, but when I do, I don't always use recursion.");
      // jokes.push("Make me a sandwich. What? Make it yourself. Sudo make me a sandwich. Okay.");
      jokes.push("Why do Java developers wear glasses? Because they can't C#.");
      // jokes.push("Can you believe Mozart wrote his 1785 Piano Concerto No. 21 in C? I can't imagine compiling back then!");
      jokes.push("A programmer had a problem, he decided to use threads. now two has . He Problems.");
      jokes.push("'Knock, knock.' 'Who's there?' *\*Very long pause.\** 'Java.'");
      jokes.push("Why was the statement scared while the comment was not? Statements are executed.");
      // jokes.push("In C we had to code our own bugs. In C++ we can inherit them.");
      jokes.push("What do you call 8 hobbits? A hobbyte.");
      jokes.push("Why do computer programmers confuse Halloween with Christmas? Because Oct 31 = Dec 25.");
      jokes.push("Why did the programmer quit her job? Because she didn’t get arrays. Har har.");
      jokes.push("Eight bytes walk into a bar. The bartender asks, 'Can I get you anything?' 'Yeah,' reply the bytes. 'Make us a double.'");
      jokes.push("Programming is like sex: One mistake and you have to support it for the rest of your life.");
      jokes.push("I had a problem so I thought to use Java. Now I have a ProblemFactory. Zing!");
      jokes.push("A project manager is a person who thinks nine women can deliver a baby in one month.");
      // jokes.push("Only me and the god knew what I was trying to do with this piece of code, when I wrote it with no comments. Now only god knows.");
      jokes.push("How many programmers does it take to change a light bulb? None, that's a hardware problem.");
      jokes.push("A SQL query goes into a bar, walks up to two tables and asks, 'Can I join you?'");
      jokes.push("So this programmer goes out on a date with a hot chick.");
      jokes.push("A woman says, 'Hey! That's private OK?!' The man hesitates for a second looking confused, then says, 'But I thought we were in the same class!'");

      // Get a random joke.
      var randomJoke = jokes[Math.floor(Math.random() * jokes.length)];

      return randomJoke;

    }

    // Initialize the timer.
    timer.init(grunt, { friendlyTime: true, color: "blue" });

    grunt.initConfig({
      // Do nothing for now.
    });

    if (grunt.cli.tasks.indexOf("joke") > -1) {

      grunt.log.ok(("Minion replies, \"Ah, straight to the good stuff, " + grunt.ghazlawl.getMinionAddressText() + ".\"")["yellow"]);

    // User is not directly running help or a joke.
    } else if (grunt.cli.tasks.indexOf("help") < 0 && grunt.cli.tasks.indexOf("joke") < 0) {

      // Vars file hasn't been specified in the command line.
      if (!grunt.ghazlawl.vars) {

        // Output the logo.
        grunt.log.writeln("  ");
        grunt.log.writeln("  ."["black"]+"█"["white"]+"█"["white"]+"█"["white"]+"▄"["white"]+"."["black"]+"▄"["white"]+"█"["white"]+"█"["white"]+"█"["white"]+"▓"["grey"]+"."["black"]+"█"["white"]+"█"["white"]+"▓"["grey"]+"."["black"]+"█"["white"]+"█"["white"]+"█"["white"]+"▄"["white"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"█"["white"]+"."["black"]+"."["black"]+"█"["white"]+"█"["white"]+"▓"["grey"]+"."["black"]+"▒"["grey"]+"█"["white"]+"█"["white"]+"█"["white"]+"█"["white"]+"█"["white"]+"."["black"]+"."["black"]+"."["black"]+"█"["white"]+"█"["white"]+"█"["white"]+"▄"["white"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"█"["white"]+"."["black"]);
        grunt.log.writeln("  ▓"["grey"]+"█"["white"]+"█"["white"]+"▒"["grey"]+"▀"["white"]+"█"["white"]+"▀"["white"]+"."["black"]+"█"["white"]+"█"["white"]+"▒"["grey"]+"▓"["grey"]+"█"["white"]+"█"["white"]+"▒"["grey"]+"."["black"]+"█"["white"]+"█"["white"]+"."["black"]+"▀"["white"]+"█"["white"]+"."["black"]+"."["black"]+"."["black"]+"█"["white"]+"."["black"]+"▓"["grey"]+"█"["white"]+"█"["white"]+"▒"["grey"]+"▒"["grey"]+"█"["white"]+"█"["white"]+"▒"["grey"]+"."["black"]+"."["black"]+"█"["white"]+"█"["white"]+"▒"["grey"]+"."["black"]+"█"["white"]+"█"["white"]+"."["black"]+"▀"["white"]+"█"["white"]+"."["black"]+"."["black"]+"."["black"]+"█"["white"]+"."["black"]);
        grunt.log.writeln("  ▓"["grey"]+"█"["white"]+"█"["white"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"▓"["grey"]+"█"["white"]+"█"["white"]+"░"["grey"]+"▒"["grey"]+"█"["white"]+"█"["white"]+"▒"["grey"]+"▓"["grey"]+"█"["white"]+"█"["white"]+"."["black"]+"."["black"]+"▀"["white"]+"█"["white"]+"."["black"]+"█"["white"]+"█"["white"]+"▒"["grey"]+"▒"["grey"]+"█"["white"]+"█"["white"]+"▒"["grey"]+"▒"["grey"]+"█"["white"]+"█"["white"]+"░"["grey"]+"."["black"]+"."["black"]+"█"["white"]+"█"["white"]+"▒"["grey"]+"▓"["grey"]+"█"["white"]+"█"["white"]+"."["black"]+"."["black"]+"▀"["white"]+"█"["white"]+"."["black"]+"█"["white"]+"█"["white"]+"▒"["grey"]);
        grunt.log.writeln("  ▒"["grey"]+"█"["white"]+"█"["white"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"▒"["grey"]+"█"["white"]+"█"["white"]+"."["black"]+"░"["grey"]+"█"["white"]+"█"["white"]+"░"["grey"]+"▓"["grey"]+"█"["white"]+"█"["white"]+"▒"["grey"]+"."["black"]+"."["black"]+"▐"["white"]+"▌"["white"]+"█"["white"]+"█"["white"]+"▒"["grey"]+"░"["grey"]+"█"["white"]+"█"["white"]+"░"["grey"]+"▒"["grey"]+"█"["white"]+"█"["white"]+"."["black"]+"."["black"]+"."["black"]+"█"["white"]+"█"["white"]+"░"["grey"]+"▓"["grey"]+"█"["white"]+"█"["white"]+"▒"["grey"]+"."["black"]+"."["black"]+"▐"["white"]+"▌"["white"]+"█"["white"]+"█"["white"]+"▒"["grey"]);
        grunt.log.writeln("  ▒"["grey"]+"█"["white"]+"█"["white"]+"▒"["grey"]+"."["black"]+"."["black"]+"."["black"]+"░"["grey"]+"█"["white"]+"█"["white"]+"▒"["grey"]+"░"["grey"]+"█"["white"]+"█"["white"]+"░"["grey"]+"▒"["grey"]+"█"["white"]+"█"["white"]+"░"["grey"]+"."["black"]+"."["black"]+"."["black"]+"▓"["grey"]+"█"["white"]+"█"["white"]+"░"["grey"]+"░"["grey"]+"█"["white"]+"█"["white"]+"░"["grey"]+"░"["grey"]+"."["black"]+"█"["white"]+"█"["white"]+"█"["white"]+"█"["white"]+"▓"["grey"]+"▒"["grey"]+"░"["grey"]+"▒"["grey"]+"█"["white"]+"█"["white"]+"░"["grey"]+"."["black"]+"."["black"]+"."["black"]+"▓"["grey"]+"█"["white"]+"█"["white"]+"░"["grey"]+" "+grunt.ghazlawl.version["grey"]);
        grunt.log.writeln("  ░"["grey"]+"."["black"]+"▒"["grey"]+"░"["grey"]+"."["black"]+"."["black"]+"."["black"]+"░"["grey"]+"."["black"]+"."["black"]+"░"["grey"]+"░"["grey"]+"▓"["grey"]+"."["black"]+"."["black"]+"░"["grey"]+"."["black"]+"▒"["grey"]+"░"["grey"]+"."["black"]+"."["black"]+"."["black"]+"▒"["grey"]+"."["black"]+"▒"["grey"]+"."["black"]+"░"["grey"]+"▓"["grey"]+"."["black"]+"."["black"]+"░"["grey"]+"."["black"]+"▒"["grey"]+"░"["grey"]+"▒"["grey"]+"░"["grey"]+"▒"["grey"]+"░"["grey"]+"."["black"]+"░"["grey"]+"."["black"]+"▒"["grey"]+"░"["grey"]+"."["black"]+"."["black"]+"."["black"]+"▒"["grey"]+"."["black"]+"▒"["grey"]+"."["black"]);
        grunt.log.writeln("  ░"["grey"]+"."["black"]+"."["black"]+"░"["grey"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"░"["grey"]+"."["black"]+"▒"["grey"]+"."["black"]+"░"["grey"]+"░"["grey"]+"."["black"]+"░"["grey"]+"░"["grey"]+"."["black"]+"."["black"]+"."["black"]+"░"["grey"]+"."["black"]+"▒"["grey"]+"░"["grey"]+"."["black"]+"▒"["grey"]+"."["black"]+"░"["grey"]+"."["black"]+"."["black"]+"░"["grey"]+"."["black"]+"▒"["grey"]+"."["black"]+"▒"["grey"]+"░"["grey"]+"."["black"]+"░"["grey"]+"."["black"]+"░"["grey"]+"░"["grey"]+"."["black"]+"."["black"]+"."["black"]+"░"["grey"]+"."["black"]+"▒"["grey"]+"░"["grey"]);
        grunt.log.writeln("  ░"["grey"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"░"["grey"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"▒"["grey"]+"."["black"]+"░"["grey"]+"."["black"]+"."["black"]+"."["black"]+"░"["grey"]+"."["black"]+"."["black"]+"."["black"]+"░"["grey"]+"."["black"]+"░"["grey"]+"."["black"]+"."["black"]+"▒"["grey"]+"."["black"]+"░"["grey"]+"░"["grey"]+"."["black"]+"░"["grey"]+"."["black"]+"░"["grey"]+"."["black"]+"▒"["grey"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"░"["grey"]+"."["black"]+"."["black"]+"."["black"]+"░"["grey"]+"."["black"]+"░"["grey"]+"."["black"]);
        grunt.log.writeln("  ."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"░"["grey"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"░"["grey"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"░"["grey"]+"."["black"]+"."["black"]+"░"["grey"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"░"["grey"]+"."["black"]+"░"["grey"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"."["black"]+"░"["grey"]+"."["black"]);

        // Output the initial message.
        grunt.log.writeln("  ");
        grunt.log.errorlns("Ye find yeself in yon dungeon unable to remember how to perform basic CSS, MySQL, and SSH functions. Ye exit through the only door to find your minion eagerly awaiting your command.");

      // User is running a task directly.
      // Format is grunt --vars=path/to/project.json task:subtask
      } else {

        grunt.log.ok(("Minion replies, \"I see you waste no time, " + grunt.ghazlawl.getMinionAddressText() + ".\"")["yellow"]);

      }

    }

    // Load the tasks.
    require("load-grunt-tasks")(grunt);
    grunt.loadTasks("tasks");

  };

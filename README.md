# Grunt Minion #

Version 2.0.3

!["Grunt Minion"](https://bytebucket.org/ghazlawl/grunt-minion/raw/74750668090eab790f989cad0f41acd1a0c12576/screenshots/screenshot-1.png)

`Grunt Minion` is a `JavaScript` task runner created with `Grunt` to help
automate repetitive tasks, such as:

* Compiling `SASS` into `CSS` using `Development` or `Production` settings.
* Syncing your local database with the server.
* Syncing your local CMS core files with the server.
* Syncing your uploads directory with the server.
* Generate documentation using `PHPDocumentor`.
* Tell you a funny (?) joke to lighten your mood.

`Grunt Minion` was written as a throwback to text-based adventure games. And,
of course, because I've always wanted a minion to do my bidding (insert deep evil
laugh here).

---

## Required Files ##

These files can live anywhere on your machine. They do not need to be placed in
the project directory.

* Gruntfile.js
    * The `Gruntfile.js` file is the core file used for task automation.
* package.json
    * The `package.json` file contains all of the information for`Node Package Manager` to keep packages up to date.
* node_modules/*
    * The `node_modules` folder contains all of the packages that are downloaded by `Node Package Manager` and are loaded by `Grunt` to run tasks.
* vars/default.json
    * The `vars/default.json` file contains a boilerplate for your `Grunt` vars file. This file should be copied and populated per remote server and named accordingly. For example, `vars/myproject.json`.

---

## Required Dependencies ##

In order to take advantage of all the features `Grunt` provides, the following
dependencies need to be installed:

* Node.js
    * [http://howtonode.org/how-to-install-nodejs](http://howtonode.org/how-to-install-nodejs)

* Node Package Manager
    * [http://howtonode.org/introduction-to-npm](http://howtonode.org/introduction-to-npm`)

* Grunt
    * [http://gruntjs.com/installing-grunt](http://gruntjs.com/installing-grunt)

* Terminal Notifier Gem
    * This can be installed by typing `gem install terminal-notifier` into your terminal.

---

## Installation ##

### Step 1 ###

Make sure that you have all of the required dependencies (above) installed.

### Step 2 ###

Type `git clone https://bitbucket.org/ghazlawl/grunt-minion .` into your
terminal to clone the `Grunt Minion` repo into an empty directory.

### Step 3 ###

Type `npm update` into your terminal to download the `Node Package Manager`
modules that are required for `Grunt Minion` to run.

### Step 4 ###

Copy the `vars/default.json` file to `vars/myproject.json`, name it appropriately,
and populate it with your project-specific values.

### Step 5 ###

Run the default `Grunt` task by navigating to the project folder and running
`grunt` in your terminal. At this point, the UI will take over.

### Step 6 ###

Profit!

### Step 6.5 ###

You can also run `grunt help` in your terminal to see a comprehensive list of
commands that you can run directly to bypass the UI (if that's your thing).

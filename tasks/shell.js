
  /**
   * @file shell.js
   * @author Jimmy K. <jimmy@ghazlawl.com>
   * @since 2.0.1
   */

  module.exports = function(grunt) {

    var shell_options = {
      stderr: false,
      execOptions: {
        maxBuffer: 1024 * 1024 * 8
      }
    };

    grunt.config("shell", {

      /* ======================================== */
      /* MySQL
      /* ======================================== */

      /* Step 2: Fetch the database from the remote server. */
      "fetch-mysql": {
        command: [
          /* Make sure the local database directory exists. */
          "mkdir -p <%= grunt.ghazlawl.vars.grunt.dirs.local %>",
          /* Copy the compressed dump file from the server to the local machine. */
          "scp <%= grunt.ghazlawl.vars.remote.user %>@<%= grunt.ghazlawl.vars.remote.host %>:./grunt-db.sql.gz ./grunt",
          /* Unzip the database dump. */
          "gunzip -f ./grunt/grunt-db.sql.gz"
        ].join("&&"),
        options: shell_options
      },

      /* ======================================== */
      /* Uploads
      /* ======================================== */

      /* Step 2: Fetch the uploads from the remote server. */
      "fetch-uploads": {
        command: [
          /* Download the tar file. */
          "scp <%= grunt.ghazlawl.vars.remote.user %>@<%= grunt.ghazlawl.vars.remote.host %>:./grunt-files.tar.gz <%= grunt.ghazlawl.vars.grunt.dirs.local %>/grunt-files.tar.gz"
        ].join('&&'),
        options: shell_options
      },

      /* Step 3: Extract the uploads. */
      "extract-uploads": {
        command: [
          /* Make sure the local uploads directory exists. */
          "mkdir -p <%= grunt.ghazlawl.vars.uploads.dirs.local %>",
          /* Extract the tar file. */
          "tar -C <%= grunt.ghazlawl.vars.uploads.dirs.local %> -zxvf <%= grunt.ghazlawl.vars.grunt.dirs.local %>/grunt-files.tar.gz"
        ].join('&&'),
        options: shell_options
      },

      /* ======================================== */
      /* Core
      /* ======================================== */

      /* Step 2: Fetch the core files from the remote server. */
      "fetch-core": {
        command: [
          /* Download the tar file. */
          "scp <%= grunt.ghazlawl.vars.remote.user %>@<%= grunt.ghazlawl.vars.remote.host %>:./grunt-core.tar.gz <%= grunt.ghazlawl.vars.grunt.dirs.local %>/grunt-core.tar.gz"
        ].join('&&'),
        options: shell_options
      },

      /* Step 3: Extract the core files. */
      "extract-core": {
        command: [
          /* Make sure the local uploads directory exists. */
          "mkdir -p <%= grunt.ghazlawl.vars.core.dirs.local %>",
          /* Extract the tar file. */
          "tar -C <%= grunt.ghazlawl.vars.core.dirs.local %> -zxvf <%= grunt.ghazlawl.vars.grunt.dirs.local %>/grunt-core.tar.gz"
        ].join('&&'),
        options: shell_options
      }

    });

  }

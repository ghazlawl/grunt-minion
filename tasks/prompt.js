
  /**
   * @file prompt.js
   * @author Jimmy K. <jimmy@ghazlawl.com>
   * @since 2.0.1
   */

  module.exports = function(grunt) {

    grunt.config("prompt", {

      "select-vars": {
        options: {
          questions: [{
            config: "value",
            type: "list",
            message: ("Minion asks, \"Which configuration file would you like to use, " + grunt.ghazlawl.getMinionAddressText() + "?\"")["yellow"],
            choices: grunt.ghazlawl.getVarsFileList()
          }],
          then: function(results, done) {
            grunt.ghazlawl.selectedFilename = results.value;
            grunt.ghazlawl.vars = grunt.file.readJSON("vars/" + grunt.ghazlawl.selectedFilename);
            grunt.log.ok(("Minion replies, \"Ah, an excellent choice, " + grunt.ghazlawl.getMinionAddressText() + ".\"")["yellow"]);
            grunt.task.run("prompt:select-action");
          }
        }
      },
      "select-action": {
        options: {
          questions: [{
            config: "value",
            type: "list",
            message: ("Minion asks, \"Which task would you like me to run, " + grunt.ghazlawl.getMinionAddressText() + "?\"")["yellow"],
            default: "\"Compile my CSS, minion.\"",
            choices: [
              "<Get ye flask.>",
              "\"Compile my CSS, minion.\"",
              "\"Watch my SASS files and compile them if anything changes.\"",
              "\"Sync the MySQL database.\"",
              "\"Sync the uploads directory.\"",
              "\"Sync the core CMS files.\"",
              "\"Sync the database, uploads, core CMS files - all of it.\"",
              "\"Document my project, minion.\"",
              "\"Tell me a joke.\"",
            ],
            filter: function(value) {

              switch (value) {
              case "<Get ye flask.>":
                return "get-ye-flask";
              case "\"Compile my CSS, minion.\"":
                return "compile-css";
              case "\"Watch my SASS files and compile them if anything changes.\"":
                return "watch-css";
              case "\"Sync the MySQL database.\"":
                return "sync-mysql";
              case "\"Sync the uploads directory.\"":
                return "sync-uploads";
              case "\"Sync the core CMS files.\"":
                return "sync-core";
              case "\"Sync the database, uploads, core CMS files - all of it.\"":
                return "sync-all";
              case "\"Document my project, minion.\"":
                return "document";
              case "\"Tell me a joke.\"":
                return "tell-joke";
              }

            }
          }],
          then: function(results, done) {

            grunt.ghazlawl.selectedAction = results.value;

            switch (results.value) {
            case "compile-css":
            case "watch-css":
              grunt.log.ok(("Minion replies, \"Absolutely, " + grunt.ghazlawl.getMinionAddressText() + ".\"")["yellow"]);
              grunt.task.run("prompt:select-env");
              break;
            case "sync-mysql":
              grunt.log.ok(("Minion replies, \"I'll fetch that for you, " + grunt.ghazlawl.getMinionAddressText() + ".\"")["yellow"]);
              grunt.task.run("sync:mysql");
              break;
            case "sync-uploads":
              grunt.log.ok(("Minion replies, \"I live to serve, " + grunt.ghazlawl.getMinionAddressText() + ".\"")["yellow"]);
              grunt.task.run("sync:uploads");
              break;
            case "sync-core":
              grunt.log.ok(("Minion replies, \"I'll give it my best, " + grunt.ghazlawl.getMinionAddressText() + ".\"")["yellow"]);
              grunt.task.run("sync:core");
              break;
            case "sync-all":
              grunt.log.ok(("Minion replies, \"Everything, " + grunt.ghazlawl.getMinionAddressText() + "? As you wish...\"")["yellow"]);
              grunt.log.ok("Minion cringes."["grey"]);
              grunt.task.run("sync:all");
              break;
            case "document":
              grunt.task.run("phpdocumentor:build", "do-notify:phpdocumentor");
              grunt.task.run("prompt:select-action");
              break;
            case "tell-joke":
              grunt.task.run("joke");
              grunt.task.run("prompt:select-action");
              break;
            case "get-ye-flask":
              grunt.log.writeln(">> "["red"] + "You can't get ye flask!"["red"]);
              grunt.task.run("prompt:select-action");
            }

          }
        }
      },
      "select-env": {
        options: {
          questions: [{
            config: "value",
            type: "list",
            message: ("Minion asks, \"And for which environment, " + grunt.ghazlawl.getMinionAddressText() + "?\"")["yellow"],
            choices: [
              "\"I'm an inexperienced nooblet, let's use the development settings so I can debug it...\"",
              "\"I'm a Level 97 Code Wizard! Let's go straight to production!\"",
            ],
            filter: function(value) {

              switch (value) {
              case "\"I'm an inexperienced nooblet, let's use the development settings so I can debug it...\"":
                return "dev";
              case "\"I'm a Level 97 Code Wizard! Let's go straight to production!\"":
                return "prod";
              }

            }
          }],
          then: function(results, done) {
            switch (results.value) {
            case "dev":
              if (grunt.ghazlawl.selectedAction == "watch-css") {
                grunt.log.ok(("Minion replies, \"I'll keep an eye on it, " + grunt.ghazlawl.getMinionAddressText() + "!\"")["yellow"]);
                grunt.task.run("css:watch:dev");
              } else if (grunt.ghazlawl.selectedAction == "compile-css") {
                grunt.log.ok(("Minion replies, \"No doubt the best option, " + grunt.ghazlawl.getMinionAddressText() + "!\"")["yellow"]);
                grunt.task.run("css:compile:dev");
                grunt.task.run("prompt:select-action");
              }
              break;
            case "prod":
              if (grunt.ghazlawl.selectedAction == "watch-css") {
                grunt.log.ok(("Minion replies, \"I'll keep an eye on it, " + grunt.ghazlawl.getMinionAddressText() + "!\"")["yellow"]);
                grunt.task.run("css:watch:prod");
              } else if (grunt.ghazlawl.selectedAction == "compile-css") {
                grunt.log.ok(("Minion replies, \"You're absolutely right, " + grunt.ghazlawl.getMinionAddressText() + "!\"")["yellow"]);
                grunt.task.run("css:compile:prod");
                grunt.task.run("prompt:select-action");
              }
              break;
            }
          }
        }
      }

    });

  };


  /**
   * @file phpdocumentor.js
   * @author Jimmy K. <jimmy@ghazlawl.com>
   * @since 1.0.3
   */

  module.exports = function(grunt) {

    grunt.config("phpdocumentor",
      {
        options: {
          command: "run"
        },
        build: {
          options: {
            directory: "<%= grunt.ghazlawl.vars.phpdocumentor.dirs.input %>",
            target: "<%= grunt.ghazlawl.vars.phpdocumentor.dirs.output %>"
          }
        }
      }
    );

  }


  /**
   * @file lineremover.js
   * @author Jimmy K. <jimmy@ghazlawl.com>
   * @since 2.0.1
   */

  module.exports = function(grunt) {

    grunt.config('lineremover', {
      "css": {
        files: [{
          expand: true,
          cwd: '<%= grunt.ghazlawl.vars.css.dirs.output %>',
          src: ['*.css'],
          dest: '<%= grunt.ghazlawl.vars.css.dirs.output %>',
        }]
      }
    });

  };

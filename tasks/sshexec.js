
  /**
   * @file sshexec.js
   * @author Jimmy K. <jimmy@ghazlawl.com>
   * @since 2.0.1
   */

  module.exports = function(grunt) {

    var ssh_remote_options = {
      host: "<%= grunt.ghazlawl.vars.remote.host %>",
      username: "<%= grunt.ghazlawl.vars.remote.user %>",
      password: "<%= grunt.ghazlawl.vars.remote.pass %>"
    };

    var ssh_vbox_options = {
      readyTimeout: 30000,
      host: "<%= grunt.ghazlawl.vars.vbox.host %>",
      username: "<%= grunt.ghazlawl.vars.vbox.user %>",
      password: "<%= grunt.ghazlawl.vars.vbox.pass %>"
    };

    grunt.config("sshexec", {

      /* ======================================== */
      /* MySQL
      /* ======================================== */

      /* Step 1: Dump the database on the remote server so we can fetch it. */
      "export-mysql": {
        command: [
          /* Create a dump of the database. */
          "mysqldump --user='<%= grunt.ghazlawl.vars.remote.mysql.user %>' --password='<%= grunt.ghazlawl.vars.remote.mysql.pass %>' <%= grunt.ghazlawl.vars.remote.mysql.db %> > ./grunt-db.sql",
          /* Zip the database dump. */
          "gzip -f ./grunt-db.sql > ./grunt-db.sql.gz"
        ],
        options: ssh_remote_options
      },

      /* Step 3: Delete the database dump to clean up after ourselves. */
      "clean-mysql": {
        command: [
          /* Remove the compressed dump file. */
          "rm -f ./grunt-db.sql.gz"
        ],
        options: ssh_remote_options
      },

      /* Step 4: Import the database into VirtualBox. */
      "import-mysql": {
        command: [
          /* Truncate the existing database tables. */
          "mysqldump -u <%= grunt.ghazlawl.vars.vbox.mysql.user %> --password=<%= grunt.ghazlawl.vars.vbox.mysql.pass %> --add-drop-table --no-data <%= grunt.ghazlawl.vars.vbox.mysql.db %> | grep ^DROP | mysql -u <%= grunt.ghazlawl.vars.vbox.mysql.user %> --password=<%= grunt.ghazlawl.vars.vbox.mysql.pass %> <%= grunt.ghazlawl.vars.vbox.mysql.db %>",
          /* Import the new data. */
          "mysql -u <%= grunt.ghazlawl.vars.vbox.mysql.user %> -p<%= grunt.ghazlawl.vars.vbox.mysql.pass %> <%= grunt.ghazlawl.vars.vbox.mysql.db %> < <%= grunt.ghazlawl.vars.grunt.dirs.vbox %>/grunt-db.sql"
        ],
        options: ssh_vbox_options
      },

      /* ======================================== */
      /* Uploads
      /* ======================================== */

      /* Step 1: Compress the uploads on the remote server so we can fetch it. */
      "compress-uploads": {
        command: [
          /* Compress the uploads file. */
          "tar -zcf ./grunt-files.tar.gz -C <%= grunt.ghazlawl.vars.uploads.dirs.remote %> ."
        ],
        options: ssh_remote_options
      },

      /* Step 4: Remove the compressed uploads file from the remote server. */
      "clean-uploads": {
        command: [
          /* Remove the compressed uploads file. */
          "rm -f ./grunt-files.tar.gz"
        ],
        options: ssh_remote_options
      },

      /* ======================================== */
      /* Core
      /* ======================================== */

      /* Step 1: Compress the core files except the uploads directory. */
      "compress-core": {
        command: [
          /* Compress the core file. */
          "tar <%= grunt.ghazlawl.vars.core.remote_exclude_pattern %> --exclude=\".git\" -zcf ./grunt-core.tar.gz -C <%= grunt.ghazlawl.vars.core.dirs.remote %> ."
        ],
        options: ssh_remote_options
      },

      /* Step 4: Remove the compress core file from the remote server. */
      "clean-core": {
        command: [
          /* Remove the compressed core file. */
          "rm -f ./grunt-core.tar.gz"
        ],
        options: ssh_remote_options
      }

    });

  }

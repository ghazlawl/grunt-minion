
  /**
   * @file comments.js
   * @author Jimmy K. <jimmy@ghazlawl.com>
   * @since 2.0.1
   */

  module.exports = function(grunt) {

    grunt.config("comments", {
      "css": {
        options: {
          singleline: true,
          multiline: true
        },
        src: ['<%= grunt.ghazlawl.vars.css.dirs.output %>/*.css'],
      }
    });

  };

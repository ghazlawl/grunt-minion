
  /**
   * @file notify.js
   * @author Jimmy K. <jimmy@ghazlawl.com>
   * @since 2.0.1
   */

  module.exports = function(grunt) {

    grunt.config("notify", {

      "css": {
        options: {
          title: "<%= grunt.ghazlawl.vars.project %>",
          message: "CSS has been compiled."
        }
      },
      "sync-mysql": {
        options: {
          title: "<%= grunt.ghazlawl.vars.project %>",
          message: "MySQL database has been synced."
        }
      },
      "sync-uploads": {
        options: {
          title: "<%= grunt.ghazlawl.vars.project %>",
          message: "Uploads have been synced."
        }
      },
      "sync-core": {
        options: {
          title: "<%= grunt.ghazlawl.vars.project %>",
          message: "Core files have been synced."
        }
      },
      "phpdocumentor": {
        options: {
          title: "<%= grunt.ghazlawl.vars.project %>",
          message: "PHPDocumentor task complete."
        }
      }

    });

  };


  /**
   * @file bless.js
   * @author Jimmy K. <jimmy@ghazlawl.com>
   * @since 2.0.1
   */

  module.exports = function(grunt) {

    grunt.config("bless", {
      "css": {
        options: {
          cacheBuster: false,
          force: true // Added in 2.0.1 because plugin was changed.
        },
        files: [{
          expand: true,
          cwd: "<%= grunt.ghazlawl.vars.css.dirs.output %>",
          src: ["*.css"],
          dest: "<%= grunt.ghazlawl.vars.css.dirs.output %>",
          ext: ".css"
        }]
      }
    });

  };

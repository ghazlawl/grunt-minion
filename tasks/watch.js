
  /**
   * @file watch.js
   * @author Jimmy K. <jimmy@ghazlawl.com>
   * @since 2.0.1
   */

  module.exports = function(grunt) {

    grunt.config("watch", {
      "css-dev": {
        options: {
          spawn: false
        },
        files: ["<%= grunt.ghazlawl.vars.css.dirs.input %>/**/*.scss"],
        tasks: ["compass:css-dev", "do-notify:css"]
      },
      "css-prod": {
        options: {
          spawn: false
        },
        files: ["<%= grunt.ghazlawl.vars.css.dirs.input %>/**/*.scss"],
        tasks: ["compass:css-prod", "comments:css", "bless:css", "lineremover:css", "do-notify:css"]
      }
    });

  };

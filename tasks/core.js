
  /**
   * @file core.js
   * @author Jimmy K. <jimmy@ghazlawl.com>
   * @since 2.0.1
   */

  module.exports = function(grunt) {

    /**
     * Run this task to make sure that Grunt is installed and working. If so,
     * you should see "This is a test message!" output to the terminal and a
     * notification in your computers notification area.
     *
     * Usage:
     * grunt test
     *
     * @author Jimmy K. <jimmy@ghazlawl.com>
     * @since 1.0.2
     */

    grunt.registerTask("test", "Run a test task.", function() {
      grunt.task.run("prompt:test");
      grunt.log.ok("This is a test message!");
    });

    /**
     * The default task that is executed if grunt is run and no task is
     * specified.
     *
     * @author Jimmy K. <jimmy@ghazlawl.com>
     * @since 2.0.1
     */

    grunt.registerTask("default", "The default task.", function() {
      grunt.log.ok(("Minion says, \"Ah, welcome back, " + grunt.ghazlawl.getMinionAddressText() + "!\"")["yellow"]);
      grunt.task.run("prompt:select-vars");
    });

    /**
     * Run this task to see a list of manual options that are available.
     *
     * Usage:
     * grunt help
     *
     * @author Jimmy K. <jimmy@ghazlawl.com>
     * #since 2.0.2
     */

    grunt.registerTask("help", "The help task.", function() {

      grunt.log.ok(("Minion says, \"Here are some of the manual tasks that you can run, " + grunt.ghazlawl.getMinionAddressText() + ".\"")["yellow"]);
      grunt.log.writeln();

      var argText = ("--vars=path/to/project.json ")["green"];
      var gruntText = ("grunt ")["grey"];

      grunt.log.writeln("Compile SASS:");
      grunt.log.writeln("- Production: " + gruntText + argText + "css:compile:prod"["blue"]);
      grunt.log.writeln("- Development: " + gruntText + argText + "css:compile:dev"["blue"]);
      grunt.log.writeln();

      grunt.log.writeln("Watch & Compile SASS:");
      grunt.log.writeln("- Production: " + gruntText + argText + "css:watch:prod"["blue"]);
      grunt.log.writeln("- Development: " + gruntText + argText + "css:watch:dev"["blue"]);
      grunt.log.writeln();

      grunt.log.writeln("MySQL:");
      grunt.log.writeln("- Sync (Export, Fetch, & Import): " + gruntText + argText + "sync:mysql"["blue"]);
      grunt.log.writeln("- Export: " + gruntText + argText + "sshexec:export-mysql"["blue"]);
      grunt.log.writeln("- Fetch: " + gruntText + argText + "shell:fetch-mysql"["blue"]);
      grunt.log.writeln("- Import: " + gruntText + argText + "sshexec:import-mysql"["blue"]);
      grunt.log.writeln();

      grunt.log.writeln("Uploads:");
      grunt.log.writeln("- Sync (Compress, Fetch, Clean, & Extract): " + gruntText + argText + "sync:uploads"["blue"]);
      grunt.log.writeln("- Compress: " + gruntText + argText + "sshexec:compress-uploads"["blue"]);
      grunt.log.writeln("- Fetch: " + gruntText + argText + "shell:fetch-uploads"["blue"]);
      grunt.log.writeln("- Clean: " + gruntText + argText + "sshexec:clean-uploads"["blue"]);
      grunt.log.writeln("- Extract: " + gruntText + argText + "sshexec:extract-uploads"["blue"]);
      grunt.log.writeln();

      grunt.log.writeln("Core (WordPress, Drupal, etc):");
      grunt.log.writeln("- Sync (Compress, Fetch, Clean, & Extract): " + gruntText + argText + "sync:core"["blue"]);
      grunt.log.writeln("- Compress: " + gruntText + argText + "sshexec:compress-core"["blue"]);
      grunt.log.writeln("- Fetch: " + gruntText + argText + "shell:fetch-core"["blue"]);
      grunt.log.writeln("- Clean: " + gruntText + argText + "sshexec:clean-core"["blue"]);
      grunt.log.writeln("- Extract: " + gruntText + argText + "sshexec:extract-core"["blue"]);
      grunt.log.writeln();

      grunt.log.writeln("Sync All:");
      grunt.log.writeln("- Sync MySQL, Uploads, & Core: " + gruntText + argText + "sync:all"["blue"]);
      grunt.log.writeln();

      grunt.log.writeln("Miscellaneous:");
      grunt.log.writeln("- Run PHPDocumentor: " + gruntText + argText + "phpdocumentor:build"["blue"]);
      grunt.log.writeln("- Help: " + gruntText + "help"["blue"]);
      grunt.log.writeln("- Joke: " + gruntText + "joke"["blue"]);

    });

    /**
     * Run this task to sync the local MySQL database with the remote database.
     * This will fetch the remote database, drop all of the tables in the local
     * database, and then import the fetched data into the local database.
     *
     * Usage:
     * grunt sync:mysql
     * grunt sync:uploads
     * grunt sync:core
     *
     * @author Jimmy K. <jimmy@ghazlawl.com>
     * @since 2.0.1
     */

    grunt.registerTask("sync", "Sync MySQL, core, or uploads.", function(a) {
      switch (a) {
      case "mysql":
        grunt.task.run("sshexec:export-mysql", "shell:fetch-mysql", "sshexec:clean-mysql", "sshexec:import-mysql");
        grunt.task.run("do-notify:sync-mysql");
        break;
      case "uploads":
        grunt.task.run("sshexec:compress-uploads", "shell:fetch-uploads", "sshexec:clean-uploads", "shell:extract-uploads");
        grunt.task.run("do-notify:sync-uploads");
        break;
      case "core":
        grunt.task.run("sshexec:compress-core", "shell:fetch-core", "sshexec:clean-core", "shell:extract-core");
        grunt.task.run("do-notify:sync-core");
        break;
      case "all":
        grunt.task.run("sync:mysql", "sync:core", "sync:uploads");
        break;
      }
    });

    /**
     * Run this task to compile your SASS into CSS. You can compile for either
     * development or production environments. Compiling for production will:
     * - Strip comments
     * - Bless CSS (break CSS files into smaller files)
     * - Remove blank lines
     *
     * Usage:
     * grunt css:watch:dev
     * grunt css:compile:dev
     * grunt css:watch:prod
     * grunt css:compile:prod
     *
     * @author Jimmy K. <jimmy@ghazlawl.com>
     * @since 2.0.1
     */

    grunt.registerTask("css", "Compile SASS into CSS.", function(a, b) {
      if (a == "watch") {
        if (b == "prod") {
          // PRODUCTION
          grunt.task.run("watch:css-prod");
        } else {
          // DEV
          grunt.task.run("watch:css-dev");
        }
      } else if (a == "compile") {
        if (b == "prod") {
          // PRODUCTION
          grunt.task.run("compass:css-prod", "comments:css", "bless:css", "lineremover:css", "do-notify:css");
        } else {
          // DEV
          grunt.task.run("compass:css-dev", "do-notify:css");
        }
      }
    });

    grunt.registerTask("do-notify", "Run tasks before notifying.", function(a) {
      switch (a) {
      case "css":
        grunt.log.ok(("Minion replies, \"A job well done if I do say so myself, " + grunt.ghazlawl.getMinionAddressText() + ".\"")["yellow"]);
        grunt.task.run("notify:css");
        break;
      case "sync-mysql":
        grunt.log.ok(("Minion replies, \"Database synced, " + grunt.ghazlawl.getMinionAddressText() + ".\"")["yellow"]);
        grunt.task.run("notify:sync-mysql");
        break;
      case "sync-uploads":
        grunt.log.ok(("Minion replies, \"Uploads synced, " + grunt.ghazlawl.getMinionAddressText() + ".\"")["yellow"]);
        grunt.task.run("notify:sync-uploads");
        break;
      case "sync-core":
        grunt.log.ok(("Minion replies, \"Core files synced, " + grunt.ghazlawl.getMinionAddressText() + ".\"")["yellow"]);
        grunt.task.run("notify:sync-core");
        break;
      case "phpdocumentor":
        grunt.log.ok(("Minion replies, \"I've documented everything I could find, " + grunt.ghazlawl.getMinionAddressText() + ".\"")["yellow"]);
        grunt.task.run("notify:phpdocumentor");
        break;
      }
    });

    /**
     * Run this task to hear a random joke.
     *
     * @author Jimmy K. <jimmy@ghazlawl.com>
     * @since 2.0.2
     */

    grunt.registerTask("joke", "Tell a joke.", function() {
      grunt.log.ok(("Minion tells you a joke, \"" + grunt.ghazlawl.getRandomJoke() + "\"")["yellow"]);
      grunt.log.ok("Minion chuckles."["grey"]);
    });

  }


  /**
   * @file compass.js
   * @author Jimmy K. <jimmy@ghazlawl.com>
   * @since 2.0.1
   */

  module.exports = function(grunt) {

    grunt.config("compass", {

      "css-dev": {
        options: {
          sassDir: "<%= grunt.ghazlawl.vars.css.dirs.input %>",
          cssDir: "<%= grunt.ghazlawl.vars.css.dirs.output %>",
          noLineComments: false,
          /* Accepted values are "expanded", "nested", "compact", "compressed". */
          outputStyle: "expanded",
        }
      },
      "css-prod": {
        options: {
          sassDir: "<%= grunt.ghazlawl.vars.css.dirs.input %>",
          cssDir: "<%= grunt.ghazlawl.vars.css.dirs.output %>",
          noLineComments: true,
          /* Accepted values are "expanded", "nested", "compact", "compressed". */
          outputStyle: "nested",
        }
      }

    });

  };
